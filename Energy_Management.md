# Energy Management

## 1. What are the activities you do that make you relax - Calm quadrant?
* I talk with my friend.
* I love to listen to some devotional music.
* Playing some mobile games.
* Talk to my mother and brother.
* perform some pranayama.
  
## 2. When do you find getting into the Stress quadrant?
* If I am capable to do some tasks and not properly doing them.
* When some bad things happen unexpectedly, which not should be.
* When I do not complete tasks on time.

## 3. How do you understand if you are in the Excitement quadrant?
* If I completed a task on time.
* When I help someone.
* When I meet my best friend.
* Find the solution to the task, which I think can not happen.

## 4. Paraphrase the Sleep is your Superpower video in detail.
* Developing a consistent sleep routine can enhance the quality of our sleep.
* According to the speaker's research, individuals who maintain regular sleep patterns can experience a  40% increase in productivity.
* Sleep is essential for the learning process both before and after acquiring new information. 
* Developing a consistent sleep routine can enhance the quality of our sleep.
* Keeping a lower body temperature and adjusting the temperature can facilitate faster sleep.
* Sleeping for only 4 hours can cause a 75% reduction in natural killer cells that aid the body in fighting illnesses.

## 5. What are some ideas that you can implement to sleep better?
* While going to sleep avoid using the electronic device.
* Sleep on time.
* Sleep for at least seven to eight hours daily.
* Do meditation before going to bed.

## 6. Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.
* Studies show that exercising can improve our mood, energy, memory, and attention. She experienced these changes.
* She started working on her health by exercising regularly and trying different physical activities.
* she realized that these activities improved her mood, motivated her to do more, and helped her stay focused on her health goals.
* She shared that exercising also improved her ability to write grants by increasing her focus.
* Studies show that exercising can improve our mood, energy, memory, and attention. She experienced these changes firsthand.
  
## 7. What are some steps you can take to exercise more?
* I complete my task on time and get some extra time to exercise.
* sleep on time, and eat healthy food for better health.
* I will give myself at least thirty minutes of exercise to myself