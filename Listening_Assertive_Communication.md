# Listening and Active Communication Assignment by Nirbhay

## 1. What are the steps/strategies to do Active Listening?
* Active Listening is an essential skill, In which full concentration is required on what someone is saying and responding accordingly.
* Try to be present in a conversation and avoid distraction.
* Show interest by making eye contact, nodding your head, and using affirmative language like "I understand"
* paraphrase in your word to make clear what the speaker is saying.
* At the end of the conversation summarize the main points.
* Active listening requires concentration, engagement, and a genuine interest in the speaker.
## 2. According to Fisher's model, what are the key points of Reflective Listening?
* Reflecting listening is a process in which we focus on the speaker's words, tone of voice, and body language to gain a complete understanding of their massage.
* Reflect back to the speaker in your word to show that you understand what the speaker is saying.
* Be emphatic in reflecting Listening and asking clarifying questions.
* Reflective listening involves actively engaging with the speaker, demonstrating that you have understood their message, and acknowledging their emotions.

## 3. What are the obstacles in your listening process.?
* Getting distracted by other things such as smartphones, or noise.
* Thinking about another thing in the mind, which is not a necessity.
* Thinking about what is the benefit of listening to this.

## 4. What can you do to improve your listening?
* I will pay full attention to the speaker and avoid distractions such as smartphones, laptops, and noise.
* I will be present in the communication and avoid thinking about other things.
* Paraphrase the word said by a speaker in our own words to make crystal clear, what the speaker is saying.
* Practice active listening regularly in our personal and professional life.
## 5. When do you switch to Passive communication style in your day to day life?
* If someone ask me for something, I give them without thinking that i also have the need for same things.
* when someone tell me wierd things about me to avoid conflict.
* Unable to give strong opinion such that i can not do this.

## 6. When do you switch into Aggressive communication styles in your day to day life?
* When someone tries to do things again and again, which irritates me.
* When people do not care about my feelings and only talk about themselves.
* When someone tries to decrease my personal rights.

## 7. When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
* I do not like Passive Aggressive Communication Styles in my day-to-day life.
* I prefer to avoid it.

## 8. How can you make your communication assertive? 
* Be clear and specific about our needs, preferences, and expectation in a concise manner.
* By showing empathy and understanding the other person's perspective.
* Avoid using aggressive or defensive language, such as yelling, blaming, and making a personal attack.
* Practice assertiveness regularly, whether in your personal or professional life.
