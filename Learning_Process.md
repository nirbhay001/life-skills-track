# Learning Process
[FS](https://fs.blog/feynman-technique/)

## 1. How to Learn Faster with the Feynman Technique
* Feynman Technique is a method of learning that increase your potential to learn new things and develop your understanding.
* Feynman technique is given by the great Nobel-prize-winning physicist Richard Feynman.The main logic behind Feynman technique that.
* choose a concept, which you want to learn wisely.
   1. Explain it to yourself, and find the mistake.
   2. organize and review yourself. 
   3. Reflect back to you to know, what mistake you have done.
* In the video, It is mentioned that, If you can not explain, It is simply because you do not understand.
* If you understand well, then you can explain it better.

## 2. What are the different ways to implement this technique in your learning process?

* I will first figure out what to learn, and why to learn.
* Understand the concept clearly and explain it in front of the mirror to yourself.
* we should keep figuring out ourself by self-evaluation.
* If you understand the concept well, explain it to someone else.

## 3. Paraphrase the video in detail in your own words.
* They explain the technique of how to rewire our brains.
*  We should understand our minds, then keep practicing the learning process.
*  In the video, it is mentioned that study for twenty-five minutes and diffuse your mind for five minutes.
* When you utilize your twenty-five minutes, avoid all distractions.
* In the rest, five minutes do the things that keep you happy.
## 4. What are some of the steps that you can take to improve your learning process?
* The first thing that i will improve in my learning process is understanding the concept well.
* Deconstruct our whole learning into chunks to make learning easier.
* Whatever I will learn, first I will explain to ourselves for better understanding.
* Do not take much pressure, while working on new things.
## 5. Your key takeaways from the video? Paraphrase your understanding.
* In the video, it is mentioned that always try to learn new things.
* There are four important points to Rapid skill Acquisation.
  1. Deconstruct the skill, which you want to learn.
  2. Learn enough to self-correct.
  3. Remove the practice barrier.
  4. Practice the skill as much as you can.
* The major barrier to skill acquisition is not intellectual, it is emotional.
## 6. What are some of the steps that you can while approaching a new topic?
* The first step to learning a new topic, we should firstly decide, how the new topic will be beneficial for me.
* Get all the resources, which will be helpful in the learning journey.
* Divide all the learning into different parts to understand the learning well.
* Set a timeline for every topic to complete on time.
* Practice more to understand the skill well.