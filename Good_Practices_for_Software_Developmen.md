# Good Practices for Software Development

## 1. What is your one major takeaway from each one of the 6 sections. So 6 points in total.
* I will always make notes while discussing requirements with our teams.
* Always communicate with your team, and inform all the relevant issues in a group channel.
* Ask and answer the question clearly, use screenshots, and diagrams and explain the solution you have tried.
* Communicate with your teammate, it will help you to improve communication and get to know your team member.
* Always be aware and mindful of another team member.
* when you work, do work with 100% involvement. When you play, play with 100% involvement. 

## 2. Which area do you think you need to improve on? What are your ideas to make progress in that area?
* Always ask for feedback I need to overcommunicate, when things are not understandable.
* Communicate with a team member, to build a stronger bond between team.
* Always ask for feedback on your work.
* Always be updated with company policy.