# Grit and Growth Mindset

## 1. Paraphrase (summarize) the video in a few lines. Use your own words.
* In the video, she mentioned how gritty individuals are more able to achieve their long-term goals.
* Gritty people are overcome by failure because they think that failure is not for their whole life.
* Gritty people have confidence in their ability to overcome obstacles and achieve their goals.
The key to gritty people is a growth mindset.

## 2. What are your key takeaways from the video to take action on?
* Believe in yourself and make yourself a gritty individual.
* I will be a gritty individual with a growth mindset.
* Grit is a way to have well-being, which also gives good direction in life.

## 3. Paraphrase (summarize) the video in a few lines in your own words.
* A growth mindset is a belief in your capacity to learn and grow.
* Changing and improving the way we people learn.
* We should always improve our skills and learn new things.
* The fixed mindset people believes that skill is born, but people with growth mindset believe that your skill will be gained by learning and practice.
* People with fixed mindsets think that they do not need to improve their skills, while people with a growth mindset believe that skills should be always improved.

## 4. What are your key takeaways from the video to take action on?
* Everything can be learned If you believe in yourself.
* Do not be afraid of failure, take the mistake and improve it.
* Do not stick at any point in life, if you facing a problem, If you have growth mindset, you can fix it.
*  Believe in yourself, you can get whatever you want in your life with hard work and patience.

## 5. What is the Internal Locus of Control? What is the key point in the video?
* The internal locus of control is the belief that everything can be done by doing hard work smartly.
* He gave the example of a child, a child who thinks they are smart and talented is the external locus of control.
Children who think that puzzles can be solved by hard work and dedication are the internal locus of control.
* People with an internal locus of control do good in their life. They can solve any problem in life by taking an interest.
People with an external locus of control do not believe in their abilities.

## 6. Paraphrase (summarize) the video in a few lines in your own words.
* The growth mindset fundamental structure is believing in your ability to figure things out, that you are facing.
* Always think that whatever skill you have, you can make it better, by learning new things.
* People with fixed mindset, believe that they can not be better because they have good knowledge.
* Always implement new good things in your life, It can be difficult at beginning of the journey but in the end, you overcome with good confidence and ability.

## 7. What are your key takeaways from the video to take action on?
* Do not be afraid at the beginning of learning a new thing, believe in yourself and you can do it.
* Improve your ability by learning new things.
* Do not stuck with the problem, find the solution and move on.
* Believe in your ability and hard work.

## 8. What are one or more points that you want to take action on from the manual? (Maximum 3)
* I will stay with a problem till I complete it. I will not quit the problem.
* I will use the weapons of Documentation, Google, Stack Overflow, Github Issues and the Internet before asking for help from fellow warriors or looking at the solution.