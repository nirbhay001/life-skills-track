# REST Architecture in detail by Nirbhay

[Javatpoint](https://www.javatpoint.com/what-is-rest)
[GFG](https://www.geeksforgeeks.org/rest-api-architectural-constraints/)
[Youtube](https://www.youtube.com/watch?v=UeAmVcqgc28)
s
REST (Representational State Transfer) is a software architectural style for building distributed systems. It is a way to create an interface through which we can access and manipulate web resources. Resources can be anything that can be represented digitally, such as a document, a video, or a user profile. A client can interact with a resource by sending a request to a server, which returns a response containing the representation of the resource.

## There are mainly five Key point in REST Architecture

### 1. Resource :-
  * Everything is a resource, It can be anything like Architecture, style, data, or functionality, these all are resources. The customer can access all the help with a request to the server.

 ### 2. URI :- 
 * URI stands for Uniform Resource Identifier  .
 * It is also called web link.
 * You can identify all the resources by URI. 
 * Each REST API resource can be accessed by using a Uniform  Resource Identifier (URI). The URI must contain the correct connection information.
 ### 3. HTTP Method :- 
 * It stads for hyper text transfer protocol 
 * HTTP is protocol designed to transfer information between networked devices.
 * The most commonly used HTTP methods are GET, POST, PUT, PATCH, and DELETE
 ### 4. Representations:-
 * When a client communicates with the server, means the client either wants to get or post the data.
 * Representation of data can be XML, JSON, or plain text.

 ### 5. Stateless :- 
 * The server does not store any type of data about the client request or client-related data.
 * The server does not store any information about previous request.
 * Stateless the system is more scalable and easier to maintain.
  
## HTTP method used in REST

### 1. GET :-
 * HTTP GET is used to retrieve data from the server.
 * GET carries the request parameters appended in the URL, and it is stored in key-value format.
 * In the GET method we can not send a large amount of data rather limited data is sent, because it is append-only 2048 characters.
 * In Google, we request the server to show me a website of amazon.
### 2. POST :- 
 * POST is used to submit new data to a server. It typically includes a request body with the data for the new resource.
 * It is commonly used for form submissions or to create new resources on the server.
 * The server typically responds to a successful POST request with a status code.
 * By POST, we fill in the specification, of what type of product we want like a cycle with a given specification.
### 3. PATCH :-
 * PATCH request is used to update a resource on the server by making partial modifications.
 * For example: I ordered a bicycle from amazon, but the front wheel is broken So, I reached out the customer service at amazon. They give me the option to replace my front wheel instead of the whole cycle that is patched.
 * It is a very useful command to send only data, that is necessary.
### 4. DELETE :- 
 * This method is used to delete a resource on the server.
 * This method requires a URI (uniform resource identifier) that identifies the resource to be deleted.
 * When a client sends a DELETE request to the server, the server removes the resource identified by the Uniform resource identifier.
 * Data in a DELETE request is encrypted and secured during transmission between the client and the server.
 * For example; I ordered a bicycle from amazon, but the front wheel is broken So, I reached customer ver. service of amazon. They give me the option to replace my front cycle instead of the front wheel instead of the whole cycle that is patched.
### 5. PUT :- 
 * PUT method is used to update an existing resource on the server.
 * It requires a URI that identifies the resource to be updated.
 * When a client sends a PUT request to the server, the server replaces the current state of the resource with the new state provided in the request body.
 * 
Layered System: The architecture is divided into layers, each of which performs a specific function.

RESTful services provide a flexible and efficient way to build web services that can be easily consumed by a wide range of clients.