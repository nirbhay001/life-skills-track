# Tiny Habits

## 1. Your takeaways from the video (Minimum 5 points)

* we should focus on making small, manageable changes.
* It takes effort and a deliberate approach to succeed.
* One way to change our behavior is by modifying our surroundings. Our environment can influence our actions.
* Changing our habits is not always easy.
* Always try to get good habits.

## 2. Your takeaways from the video in as much detail as possible

* BJ Fogg figured out the formula for how people behave: B=MAP (Behaviour equals Motivation, Ability, and Prompt).
* Tiny Habit is a method that can help us make big changes in our life.
* We should start with tiny habits that do not need a lot of motivation to do, to help us get going.
* We should keep our tiny habits going and reward ourselves for completing them.

## 3. How can you use B = MAP to make making new habits easier?
  
* The formula for human behavior is called B = MAP, which stands for Behavior = Motivation Ability Prompt. This formula tells us that three things are necessary for us to change our behavior: motivation, ability, and promptness.
* Motivation is the desire to change a behavior or start a new one. Ability means that we are capable of making the change. 

## 4. Why it is important to "Shine" or Celebrate after each successful completion of a habit?
* Celebrating after completing a habit is very important for changing behavior.
* It is the best way to keep ourselves motivated. When we celebrate completing a small habit, it gives us a sense of achievement and encourages us to continue. 
* If we keep repeating the habit for a long time, it will become a part of our routine.



## 5. Your takeaways from the video (Minimum 5 points),(Better Every day Video)

* Small improvements can add up to a big change over time
* Good habits help us manage our time better, while bad habits make us waste time
* Being motivated alone cannot guarantee long-term success
* Our habits are influenced by our surroundings and the people we spend time with
* Consistency is more important than doing something perfectly to develop a habit

## 6. Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?

* Good habits help us manage time effectively, while bad habits waste our time
* It is important to focus on building habits, rather than just setting goals
* Changing our identity is key to changing our habits
* The immediate results we see from our habits are just the surface level of the change we are making

## 7. Write about the book's perspective on how to make a good habit easier?

* We can turn a behavior into a good habit by seeing it as part of our identity
* A cue is a trigger that makes our brain start a behavior
* We feel motivated to do the behavior because we have a craving for the reward
* The behavior we do is the response
* These four elements - cue, motivation, response, and reward - are the keys to changing our behavior.

## 8. Write about the book's perspective on how to make a good habit easier?

* To avoid bad habits, we can change our environment to make them less obvious
* Practice good habits on a routine basis, It will help to grow them.
* Making a habit more difficult to do by adding friction can help us break the habit

## 9. Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

* I will go for walk and exercise on daily basis.
* I will be waking up early so that I can manage my time accordingly.
* I will monitor my step count daily.

## 10. Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

* I can write down what time I wake up on weekends and put the note on my desk to remind myself of my goal
* To motivate myself to wake up early, I can use a reward system. Conversely, if I fail to wake up early, I can use a punishment mechanism to help break the habit.