# Prevention of Sexual Harassment

## 1. What kinds of behavior cause sexual harassment?
*  Unwanted physical contact including touching, hugging, or kissing, that is unwanted and non-consensual can be considered sexual harassment.
*  Making sexual comments, jokes, or gestures that are unwelcome and make someone feel uncomfortable.
*  Displaying or sharing sexually explicit photos, videos, or material in the workplace.
*  Unwanted sexual advances, including requests for sexual favors, can be considered sexual harassment.
*  Using power or authority to make unwanted sexual advances.

## 2. What would you do in case you face or witness any incident or repeated incidents of such behavior?
* If you feel that person's behavior is unwelcome and appropriate, speak up and let the person know.
* Document the incident by making a note of the date, time, location, and details of the incident.
* Report the incident to the respected authority and nearest police station.
* Talk to a trusted friend, or family member and tell them about the incident.
* Take care of yourself because sexual harassment can take a toll on your mental and emotional well-being. practice self-care activities such as exercise, meditation, or spending time with loved ones.
  
  